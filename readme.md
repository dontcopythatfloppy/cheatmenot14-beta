# About FIFA15

I'm sorry guys but I haven't bought it so I won't work on a new tool.

# CheatMeNot14 BETA
[TOC]

## Introduction

![Ronaldo's price comparison](http://i.imgur.com/DsZhE9h.jpg)

Hi, if you're here you probably know that the PC version of FIFA 14 Ultimate Team is plagued by cheaters. This tool helps you finding real players and avoiding bots and cheaters in hope to save time and contracts. 
Any kind of feedback is appreciated, if you find my tool useful and you feel like showing me your appreciation [I'd gladly accept a beer from you. Food and games are fine too, especially those without cheaters](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=GPJYRAUTA2FZW&lc=GB&item_name=CheatMeNot14&item_number=CHMN14&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted). :)

Also please note that this tool makes FUT somewhat playable but doesn't solve the issue, for a real solution please [contact EA](http://help.ea.com/en/contact-us/).

## FIFA Ultimate Team World Cup patch
Any CheatMeNot14 version prior to BETA4-3 won't work anymore, download the latest release.

## Video How-To
* Extremely short demo of the new features: https://www.youtube.com/watch?v=yTrtqjorX6Y (BETA4-2)
* General instructions: https://www.youtube.com/watch?v=p2YOczx_5sQ (BETA2)

## Releases
* **BETA4-3 is out** (30/05/2014 13:00 GMT+1)
* BETA4-2 is out (20/05/2014 19:30 GMT+1)
	* ![BETA4-2 features preview](http://i.imgur.com/P4PosCf.jpg)
* BETA4-1 is out (15/05/2014 17:00 GMT+1)
* BETA4 is out (12/05/2014 19:00 GMT+1)
* BETA3 is out (11/05/2014 20:00 GMT+1)
* BETA2 is out (09/05/2014 04:30 GMT+1)
* BETA1 is out (08/05/2014 03:00 GMT+1)


The changelog has been moved [here](https://bitbucket.org/dontcopythatfloppy/cheatmenot14-beta/src).

## Is this safe? Will you steal my passwords?
This tool doesn't require elevated privileges to run and doesn't need network access. [Block it on your firewall if you're paranoid](http://www.technipages.com/blockunblock-programs-in-windows-firewall).
Check the [virustotal.com scan results](https://www.virustotal.com/it/url/85a78363cc80e494634929f51a73bd9793ac91ccb599bf270bfb53f982d4129e/analysis/1400014981/): 0/52.
Also please note that **my OriginID and FUT Club are very much exposed in every video**, I have more than 750 FUT14 matches on that account and a pretty decent team, I wouldn't distribute malware with my main account, that would be too stupid.

## What is this tool about?

Cheaters usually don't even bother putting together proper teams, they usually have random squads, with low rating and chemistry, bronze players, etc.
**This tool analyzes your opponent's club and formation** in hope to spot something wrong and warns you about that **before the match starts**.
Backing off before the loading screen doesn't result in a waste of contracts, and you avoid wasting your time in pointless loading screens or buying/reapplying contracts.
Since the latest release my tool can also open for you your opponent's profile on EA's website so you can check if they have a legitmate winrate and match history.

## TL;DR How does i use it?

1. Open FIFA 14 and get in Ultimate Team.
2. Switch FIFA in windowed mode (ALT+Enter) and resize it to be a little smaller.
3. Open CheatMeNot14 (and resize it to appropriately), run "CheatMeNot14--auto-open-profile.bat" if you want it to automatically open your opponent's profile in your browser (you must be logged in @ http://www.easports.com/fifa/football-club or the stats will be unavailable.)
4. Start a match.
5. Read the warnings before and after selecting the match kits.
6. Read them carefully.
7. Decide what do to. Not every warning is meaningful (ie. playing a CM as CDM raises a warn but it's something very normal), you have to check the context.
    * Press ALT+c if you want to manually open your opponent's profile in your browser.
	* If you decide to play, select FIFA14's window and switch back to fullscreen (ALT+Enter).
8. Whitelist (ALT+w) real players and blacklist (ALT+b) the cheaters.
    * You can do this at any time, even during/after the match, as long as their informations are still displayed.

## Someone is most likely a cheater if
* you play against him and he disconnects immediatly or after an own goal.
* his team has very low chemistry or rating.
* there are lots of bronzes/untradeables in his formation.
* there are lots of non-rare gold players in his formation (and he has low chemistry).
* he has no goalkeeper.
* his formation is a mess.
* he has many basic chemistry style players.
* his squad is extremely high rated and his established date is very recent.
* he's very quick at setting ready and selecting his kit.
* his winrate on is extremely high.
* **his pass accuracy is extremely low**, most effective way so far.
* he has too many matches for his estabilished date.
* he has multiple reports of matches won without shooting, passing or even touching the ball.

[A gallery of cheaters](http://imgur.com/a/Gavkd). 
[Even more cheaters](http://imgur.com/a/gusLU).

When a legit disconnection occurs the game hangs for few seconds in attempt to re-establish the connection, cheaters disconnect immediatly, try not to blacklist ragequitters or people with bad connections.

## Blacklisting and whitelisting?
Not every cheater has a random team, **the ones with proper teams are undetectable and won't raise any warning.**
Blacklist those sneaky cheaters to prevent getting fooled twice.
Whitelist who doesn't cheat but please not that most cheaters have more than one account so try to not whitelist those who have majestic teams on a young account.

At the moment the only way to remove entries from both those lists is editing "CheatMeNot14.json" by hand.
Blacklist/whitelist someone only if you're really sure of what you're doing, I will improve these basic functionalities in BETA5.

Those lists are stored locally, but I will try my best to collect informations from few trusted users and distribuite a huge list of cheaters.
When updating do not overwrite your "CheatMeNot14.json" if you don't want to overwrite your blacklist and whitelist with mine.
 
## What do you mean by "cheater" and what are they doing?

Check the Ultimate Team Top 100 leaderboards, **everyone on there is a cheater or a coin farming bot**.
You can tell by checking their winrate and playtimes, the first one has around 11.000 victories out of 12.000 matches, that sums up to like 7 months of 24/h playing. 

FUT Servers believe what the cheaters say because REASONS, you get disconnected after the loading screen and they win coins and bonuses.

The Top 100 teams alone have farmed more than one billion coins. There are probably thousands of them.
This has severe implications on the game, one of those is that **it can take up to 30 minutes to find a real opponent in division 1**, that's why this tool exists.

![Ibra prices](http://i.imgur.com/QyoooiQ.jpg)

Have a look at "cheaters.txt" under "Source" to get a grasp on how many cheaters are out there. (If you find your name on your list and you think I've made a mistake please send me an email.)

I've been matched with more than literally hundreds of cheaters in just few months. Like 500. Let's say that selecting the match kits, the loading screen, the optional intro, the optional kickoff, returning to season menu, reapplying and eventually buying contracts takes 2 minutes on average. It would sum up to **16 hours wasted** if I didn't avoid most of them.

## Known issues
* Untradeable players have no tier, there's no practical way around this, tiers are determined by the quicksell value of that player, untradeables quicksell for 0.
* The chemistry value shown in the tool is the lower one that gets shown under "squad selector", so it's rarely 100, there's no way around this.
* If you get matched against the same guy multiple times in a row the tool won't check him again. 
 
# The whole cheaters situation
* FIFA 12 was broken and full of cheaters. https://www.youtube.com/watch?v=-09KlkcJpbo
* FIFA 13 was broken and full of cheaters. https://www.youtube.com/watch?v=lfKOR_e-1VU
* FIFA 14 is broken and full of cheaters.

It's the same flaw, **It's a port problem**, on consoles you can trust clients because one would need a modded console in order to mess with its memory but going online with one of those would get it banned from the network.
**PCs don't have that, you're free to do whatever you want with your memory**, it has always been like this and I really hope that it will always be. **Trusting clients in a multiplayer game on PC is** as **wrong** as adding a big red "Instant Win!" button in the middle of the screen.

Since the flaw is conceptually always the same, **professionals can hack the game in no time**. [FIFA14 got hacked few hours after its launch](http://forum.ea.com/uk/posts/list/90/2577535.page).
Those exploits are initially kept private because that's how coin sellers earn money, but on the long run common people catch up, public tools are released and they slowly start spreading.

Meanwhile those who started cheating early can use their coins to monopolize the in-game market and earn even more coins, ruining the game for everyone else.
At this point everything is already utterly broken and unfair but it's not really noticeable until the public exploits get widespread.

![Messi price comparison](http://i.imgur.com/Vjp7JH3.jpg)

Servers shouldn't believe a client that tells "I've won 5 - 0" before the kickoff. **Comparing the results for every match would most likely solve the issue**, you collect the result from both players (if possible) and if it's different you flag them both as potential cheaters. A cheater/bot would get flagged after every match, it would be really easy at that point to track him down and ban him.
**Those who buy coins should be punished**, it happens with trades that can be tracked, cheap items sold for millions is something that should trigger an alarm. There's a reason why EA sells FIFA Points instead of coins and it's to keep the game fair, if someone else sells them everything falls apart.

**It's too late for FIFA 14 FUT on PC to be redeemed**, billions of coins have been farmed and spent, the market is bananas and cheaters are using multiple accounts to keep their main one safe. 
Cheaters earn coins 20 times faster than legit players, if you're not one of them or a very lucky and skilled trader you can't outbid them on any rarity they want.

Note that I'm focusing on Ultimate Team but other modes like the regular seasons are/were cheatable as well.

## Why isn't EA reacting?

I'm guessing that the real issue here is that FIFA games are designed around and for consoles, the PC market share is too small to make EA act since apparently everyone is still buying the game anyway.
While there are potentially easy fixes that could be implemented, **like requiring to fill a captcha to enter the matchmaking queue**, fixing this issue for good requires a complete overhaul of the multiplayer and networking logics. It's a huge work and it costs a lot.

## Market bots

They are a whole different issue but [they do exist](http://forum.ea.com/uk/posts/list/2880445.page) and cause great harm to the market prices and they hog the servers, flooding them with requests.
There are no really user-friendly solutions here, requiring to fill a simple captcha in order to access the market search function could solve the issue.

# Can your tool be used for cheating?
Not really. Someone could use it to avoid players with a high winrate, that would classify as cheating, but:

1. You already have to avoid cheaters, it takes time, avoiding also anyone better than you would take ages.
2. It's rare to get matched with the same guy twice, real players don't enter the matchmaking as often as bots/cheaters do. Cheaters start new matches every minute or so, real players do that every ~25 minutes.
3. There are tools that let you win games without playing, why would someone bother cherry picking with mine?
4. You can already cherry-pick your opponents by intentionally staying in the lower divisions.
	
# Feedback and contacts
* thatfloppy (at) gmail (dot) com
* http://www.reddit.com/r/FIFA/comments/25e80s/fifa14_pc_ultimate_team_cheaters_a_tool_to_avoid/
* http://www.reddit.com/r/Games/comments/25dd5h/due_to_eas_unwillingness_to_deal_with_the/
* [buy me a beer, food or games without cheaters](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=GPJYRAUTA2FZW&lc=GB&item_name=CheatMeNot14&item_number=CHMN14&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted) :)

# Final notes
It's very unlikely that you will get banned for using this anti-cheater tool (oh the irony), yet I can't guarantee anything, EA's position on this tool is: **downloading is at your own risk and it's not endorsed by EA**.

![EA forum post](http://i.imgur.com/tqTJjFQ.jpg)
http://forum.ea.com/uk/posts/list/990/2577535.page
 
  
Hope this helps you save some time and contracts, have fun.

Also a huge "thank you" to eur0pa/infausto, the author of the [Dark Souls PVP Watchdog](https://bitbucket.org/infausto/dswatchdog-beta), [he taught me everything i needed to make this tool](http://www.reddit.com/r/Games/comments/25dd5h/due_to_eas_unwillingness_to_deal_with_the/chg5aum).